---
title: "Noordine"
image: "images/author/noordine.webp"
email: "hellonoorhere@gmail.com"
date: 2023-09-15
draft: false
social:
- icon: "la-gitlab"
  link: "https://gitlab.com/noordine"
- icon: "la-linkedin-in"
  link: "https://www.linkedin.com/in/mouhamed-noordeen-69b85b247/"
---

I am an alumnus of Pondicherry University. I've been part of the Free Software Hardware Movement Puducherry for six years. Currently, I'm focused on creating open source, decentralized, community-owned knowledge archiving platforms. These platforms enable communities to archive media, annotate fragments, and share knowledge collaboratively. We're transitioning our existing decentralized platform into a federated model for increased accessibility and collaboration. Additionally, we're exploring the use of IPFS to ensure data integrity and prevent censorship or loss. I'm also actively involved in advancing decentralized community mesh networks.
