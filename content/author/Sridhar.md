---
title: "Sridhar"
image: "images/author/sridhar.webp"
email: "sridhar79.rgss@proton.me"
date: 2023-08-29
draft: false
social:
- icon: "la-gitlab"
  link: "https://gitlab.com/SridharRG"
- icon: "la-linkedin-in"
  link: "https://www.linkedin.com/in/sridhar-r-a46735270/"
- icon: "lar la-comments"
  link: "https://matrix.to/#/@bixxzy326:matrix.org"
---

Heyyy, I'm Sri.
Love to learn and I love to tell the world about what I learn
